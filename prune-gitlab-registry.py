#!/usr/bin/env python3.7
import argparse
import io
import tarfile
import urllib

import docker
import requests


dockerfile = b"""
FROM scratch
ENV _ _
"""


def get_docker_image_id(client):
    fileobj = io.BytesIO()
    with tarfile.open(fileobj=fileobj, mode="w:gz") as tar:
        info = tarfile.TarFile.tarinfo(name="Dockerfile")
        info.size = len(dockerfile)
        tar.addfile(info, io.BytesIO(dockerfile))
    fileobj.seek(0)
    image, logs = client.images.build(custom_context=True, fileobj=fileobj)
    [*logs]
    return image


def iterate_tags(api, project, registry, token):
    headers = {"Private-Token": token}
    url = f"{api}/projects/{urllib.parse.quote(project, safe='')}/registry/repositories"
    response = requests.get(url, headers=headers)
    assert 200 <= response.status_code < 300, response.json()
    registry_id = None
    for r in response.json():
        if r["name"] == registry:
            registry_id = r["id"]
            break
    assert registry_id, f"Unknown registry {registry} for project {project}"
    url += f"/{registry_id}/tags"
    next_url = url
    while next_url:
        response = requests.get(next_url, headers=headers)
        assert 200 <= response.status_code < 300, response.json()
        yield from response.json()
        next_url = response.links.get("next", {}).get("url")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--project", help="The GitLab project for which to prune images.", required=True
    )
    parser.add_argument(
        "--registry", help="The GitLab registry for which to prune images.", default=""
    )
    parser.add_argument(
        "--username",
        help="The username used for authenticating to the GitLab registry.",
        required=True,
    )
    parser.add_argument(
        "--token",
        help="The token used for authenticating to GitLab API and registry.",
        required=True,
    )
    parser.add_argument(
        "--api", help="THe GitLab REST API to use.", default="https://gitlab.com/api/v4"
    )
    args = parser.parse_args()
    client = docker.from_env()
    replacement = get_docker_image_id(client)
    print("Built image", replacement.id)
    for image in iterate_tags(args.api, args.project, args.registry, args.token):
        print("Tagging", replacement.id, "as", image["location"])
        assert replacement.tag(
            image["location"]
        ), f"Failed to create tag {image['location']}"
        for message in client.images.push(
            image["location"],
            stream=True,
            decode=True,
            auth_config={"username": args.username, "password": args.token},
        ):
            print(message)
    client.images.remove(replacement.id, force=True)
    print("Deleted replacement image", replacement.id)


if __name__ == "__main__":
    main()
