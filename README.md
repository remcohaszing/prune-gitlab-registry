# Prune GitLab Registry

> **CAUTION**: This script **will replace** all images in the specified Docker registry!

This is a small Python script which works around gitlab-org/gitlab-ce#45495. It does so by building and pushing a 0 bytes Docker image to replace all images in the registry. After all images have been replaced, the registry can be deleted manually from the registry page of the GitLab project.

## Requirements

- Python 3.7
- [Pipenv][]
- Docker

## Installation

Clone the project, and install it.

```sh
pipenv sync
```

## Usage

Create a personal access token in the [GitLab profile page][] and grant it the `api` scope. Preferably, set an expiration date in the near future.

Simply run the `clean.py` script using pipenv.

```sh
pipenv run ./prune-gitlab-registry.py --token <token> --username <gitlab-username> --project <path/to/project>
```

[gitlab profile page]: https://gitlab.com/profile/personal_access_tokens
[pipenv]: https://pipenv.readthedocs.io/en/latest/
